#!/bin/bash
#BDHM
DEBUG=true;
# Ex: debug "Hello World"
debug() {
  tmpTxt=${1:-"Empty value. Please check!"}
  if [ "$DEBUG" = true ]; then
    tmpCurrentTime=`date '+%Y-%m-%d %H:%M:%S'`
    echo  "[DEBUG ${tmpCurrentTime}]: $tmpTxt"
  fi
}
# removeLineContent $path $lineContent
function removeLineContent() {
  lineContent=$2
  filePath=$1
  if [ -n "$(grep $lineContent $filePath)" ]; then
    debug "$lineContent is founded in your $filePath, Removing now..."
    sed -i".bak" "/$lineContent/d" $filePath
  fi
}
# myunzip $filePath
function myUnzip() {
  debug "Waiting for extracting $1 ..."
  unzip -qqo $1
}